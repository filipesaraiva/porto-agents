
public enum ServicesPortoAgents {
	
	/*
	 * Máquinas para carga/descarga de barcos
	 */
	Maquina("Máquina", 0),
	
	/*
	 * Docas para atracagem de barcos
	 */
	Doca("Doca", 1);
	
	private final String service;
	private final Integer code;
	
	private ServicesPortoAgents(String service, Integer code) {
		this.service = service;
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String toString() {
		return service;
	}
}
