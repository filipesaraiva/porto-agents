import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;

public class PortoEscutaMensagensBehaviour extends CyclicBehaviour{
	
	public PortoEscutaMensagensBehaviour(Agent agente) {
		super(agente);
	}
	
	@Override
	public void action() {
		try {
			ACLMessage message = myAgent.receive();
			
			switch(ProtocolMessagesPortoAgents.getCodeFromString(message.getProtocol())) {
				case 0:
					barcoProcuraDoca(message);
					break;
					
				default:
					System.out.println("Protocolo de mensagem incorreto\nCódigo enviado: " +
							ProtocolMessagesPortoAgents.getCodeFromString(message.getProtocol()));
					break;
			
			}
		} catch (Exception e) {
			block();
		}
	}
	
	private void barcoProcuraDoca(ACLMessage message) {
		System.out.println("Mensagem de " + message.getSender().getLocalName());
		System.out.println("Verificando docas disponíveis");
		
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType(ServicesPortoAgents.Doca.toString());
		
		template.addServices(sd);
		
		try {
			DFAgentDescription [] result = DFService.search(myAgent, template);
			
			ACLMessage buscaDoca = new ACLMessage(ACLMessage.REQUEST);
			buscaDoca.setProtocol(ProtocolMessagesPortoAgents.HA_DOCAS_LIVRES.toString());
			
			for (DFAgentDescription d : result) {
				System.out.println(d.getName());
				buscaDoca.addReceiver(d.getName());
			}
			
			myAgent.send(buscaDoca);
			
			int numDocasReplies = 0;
			ArrayList docas = new ArrayList();
			while (numDocasReplies < result.length) {
				ACLMessage docaReply = myAgent.receive(MessageTemplate.or(
						MessageTemplate.MatchProtocol(ProtocolMessagesPortoAgents.DOCA_LIVRE.toString()),
						MessageTemplate.MatchProtocol(ProtocolMessagesPortoAgents.DOCA_OCUPADA.toString())));
				
				if (docaReply != null) {
					numDocasReplies++;
					if (docaReply.getProtocol().equals(ProtocolMessagesPortoAgents.DOCA_LIVRE.toString())) {
						docas.add(docaReply.getSender());
					}
				} else {
					block();
				}
			}
			
			if (!docas.isEmpty()) {
				ACLMessage replyBarco = message.createReply();
				replyBarco.setProtocol(ProtocolMessagesPortoAgents.DOCA_PARA_ATRACAGEM.toString());
				replyBarco.setContent(((AID)docas.get(0)).getLocalName());
				
				ACLMessage replyDoca = new ACLMessage(ACLMessage.INFORM);
				replyDoca.addReceiver(((AID)docas.get(0)));
				replyDoca.setProtocol(ProtocolMessagesPortoAgents.DOCA_PARA_ATRACAGEM.toString());
				replyDoca.setContent(message.getSender().getLocalName());
				
				myAgent.send(replyBarco);
				myAgent.send(replyDoca);
				
				System.out.println("Enviando barco " + message.getSender().getLocalName() +
						" para doca " + ((AID)docas.get(0)).getLocalName());
			} else {
				ACLMessage replyBarco = message.createReply();
				replyBarco.setProtocol(ProtocolMessagesPortoAgents.SEM_DOCA_PARA_ATRACAGEM.toString());
				myAgent.send(replyBarco);
				
				System.out.println("Sem doca para o barco " + message.getSender().getLocalName());
				((Porto) myAgent).barcosAguardando.add(message.getSender());
			}
			
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
}
