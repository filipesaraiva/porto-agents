
public enum ProtocolMessagesPortoAgents {
	
	/*
	 * Protocolo quando um barco pergunta se há docas para atracar e, caso sim,
	 * retorna o inteiro com a localização da doca; caso não haja, espera-se
	 * o retorno de um número -1
	 */
	ONDE_E_A_DOCA("Onde é a doca", 0),
	
	/*
	 * Protocolo quando perguntado se há docas livres
	 */
	HA_DOCAS_LIVRES("Há docas livres", 1),
	
	/*
	 * Protocolo quando a doca está ocupada
	 */
	DOCA_OCUPADA("Doca ocupada", 2),
	
	/*
	 * Protocolo quando a doca está livre
	 */
	DOCA_LIVRE("Doca livre", 3),
	
	/*
	 * Protocolo que indica a doca que o barco deverá atracar
	 */
	DOCA_PARA_ATRACAGEM("Doca para atracagem", 4),
	
	/*
	 * Protocolo que diz não há docas para o barco atracar
	 */
	SEM_DOCA_PARA_ATRACAGEM("Sem doca para atracagem", 5);
	
	private final String protocolMessage;
	private final Integer code;
	
	private ProtocolMessagesPortoAgents(String protocolMessage, Integer code) {
		this.protocolMessage = protocolMessage;
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public static Integer getCodeFromString(String protocolMessage) {
		for(ProtocolMessagesPortoAgents p : ProtocolMessagesPortoAgents.values()) {
			if (p.toString().contentEquals(protocolMessage)) {
				return p.getCode();
			}
		}
		return -1;
	}
	
	public String toString() {
		return protocolMessage;
	}

}
