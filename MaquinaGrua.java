import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class MaquinaGrua extends Agent {
	
	@Override
	protected void setup() {
		ServiceDescription sd = new ServiceDescription();
		sd.setType(ServicesPortoAgents.Maquina.toString());
		sd.setName(ServicesPortoAgents.Maquina.toString());
		
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
