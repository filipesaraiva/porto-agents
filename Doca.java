import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class Doca extends Agent {
	
	private Boolean ocupada = false;
	
	@Override
	protected void setup() {
		ServiceDescription sd = new ServiceDescription();
		sd.setType(ServicesPortoAgents.Doca.toString());
		sd.setName(ServicesPortoAgents.Doca.toString());
		
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		addBehaviour(new DocaEstadoBehaviour(this));
	}

	public Boolean getOcupada() {
		return ocupada;
	}

	public void setOcupada(Boolean ocupada) {
		this.ocupada = ocupada;
	}
	
}
