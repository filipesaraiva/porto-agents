import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class DocaEstadoBehaviour extends CyclicBehaviour {
	
	public DocaEstadoBehaviour(Doca doca) {
		super(doca);
	}
	
	@Override
	public void action() {
		try {
			ACLMessage message = myAgent.receive();
			
			switch(ProtocolMessagesPortoAgents.getCodeFromString(message.getProtocol())) {
				case 1:
					ACLMessage resposta = message.createReply();
					if (((Doca) myAgent).getOcupada()) {
						System.out.println("Doca " + myAgent.getAID().getLocalName() + " ocupada");
						resposta.setProtocol(ProtocolMessagesPortoAgents.DOCA_OCUPADA.toString());
					} else {
						System.out.println("Doca " + myAgent.getAID().getLocalName() + " livre");
						resposta.setProtocol(ProtocolMessagesPortoAgents.DOCA_LIVRE.toString());
					}
					myAgent.send(resposta);
					break;
					
				case 4:
					System.out.println("Doca " + myAgent.getLocalName() + " será ocupada por barco " +
							message.getContent());
					((Doca)myAgent).setOcupada(true);
					break;
					
				default:
					System.out.println("Protocolo de mensagem incorreto\nCódigo enviado: " +
							ProtocolMessagesPortoAgents.getCodeFromString(message.getProtocol()));
					break;
			
		}
		} catch (Exception e) {
			this.block();
		}
	}

}
