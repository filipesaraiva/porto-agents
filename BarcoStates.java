import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class BarcoStates extends CyclicBehaviour{
	
	private Integer state = 0;
	
	public BarcoStates(Barco agente) {
		super(agente);
	}
	
	@Override
	public void action() {
		switch (state) {
			case 0:
				ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
				message.setProtocol(ProtocolMessagesPortoAgents.ONDE_E_A_DOCA.toString());
				message.addReceiver(new AID("porto", AID.ISLOCALNAME));
				myAgent.send(message);
				
				/*
				 * state muda para 1 para aguardar chegada da mensagem de resposta
				 */
				state = 1;
				break;
			case 1:
				ACLMessage reply = myAgent.receive(MessageTemplate.or(
						MessageTemplate.MatchProtocol(ProtocolMessagesPortoAgents.DOCA_PARA_ATRACAGEM.toString()),
						MessageTemplate.MatchProtocol(ProtocolMessagesPortoAgents.SEM_DOCA_PARA_ATRACAGEM.toString())));
				
				if (reply != null) {
					if (reply.getProtocol().contentEquals(ProtocolMessagesPortoAgents.DOCA_PARA_ATRACAGEM.toString())) {
						System.out.println("Barco " + myAgent.getLocalName() + " indo para doca " + reply.getContent());
					} else {
						System.out.println("Sem docas para barco " + myAgent.getLocalName() + " - aguardar");
					}
					state = -1;
				} else {
					block();
				}
				break;
			default:
				break;
		}
	}

}
